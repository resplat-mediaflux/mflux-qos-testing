# -*- coding: utf-8 -*-
#!/usr/bin/python
import send_to_carbon
import os
import timeit
import configparser
import io
import time
import logging
import shutil

# Load configuration file
config = configparser.ConfigParser()
config.read("qos_test.ini")

# Get setup information from ini file
testFile = config.get('qostest', 'testFile')
prefix = config.get('qostest','prefix')
uuid = config.get('qostest','uuid')
namespace = config.get('qostest','namespace')
smbpath = config.get('qostest','smb_mount')
locality = config.get('qostest','probe_id')

graphPrefix = prefix+uuid+".qos."+locality+"."

testfilesize = os.path.getsize(testFile)

try:
    def copyto():
        shutil.copy(src=testFile, dst=smbpath)

    copyttime = timeit.timeit(copyto,number=1)
    copytorate = (testfilesize/copyttime)

    # Build tuple for (copy to smb)
    now = int(time.time())
    tuples = ([])
    tuples.append((str(graphPrefix + 'smb.copyto.size.bytes'), (now, str(testfilesize))))
    tuples.append((str(graphPrefix + 'smb.copyto.time.sec'), (now, str(copyttime))))
    tuples.append((str(graphPrefix + 'smb.copyto.speed.bs'), (now, str(copytorate))))

    print(tuples)
    # Send to carbon server
    send_to_carbon.sendtocarbon(tuples)

    fname = testFile.split("/")
    def copyfrom():
        shutil.copy(src=smbpath+"/"+fname[-1], dst=testFile)

    copyftime = timeit.timeit(copyfrom, number=1)
    copyfromrate = (testfilesize/copyftime)
    print(copyftime)

    # Build tuple for (copy from smb)
    now = int(time.time())
    tuples = ([])
    tuples.append((str(graphPrefix + 'smb.copyfrom.size.bytes'), (now, str(testfilesize))))
    tuples.append((str(graphPrefix + 'smb.copyfrom.time.sec'), (now, str(copyftime))))
    tuples.append((str(graphPrefix + 'smb.copyfrom.speed.bs'), (now, str(copyfromrate))))

    print(tuples)
    # Send to carbon server
    send_to_carbon.sendtocarbon(tuples)

finally:
    try:
        myfile = smbpath+"/"+fname[-1]
        os.remove(myfile)
    except OSError as e: ## if failed report ##
        logging.warning("Failed to delete copied file. Exception thrown: " + str(e.filename,e.sterror))
