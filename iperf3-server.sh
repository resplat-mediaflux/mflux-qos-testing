#!/bin/bash
# Run the iperf server, and if it has not been contacted after 5 minutes, kill it
timeout 5m iperf3 -s -p 8080 -1 > /dev/null

# if iperf3 supports authentication, use this version:
#timeout 5m iperf3 -s --authorized-users-path credentials.csv --rsa-private-key-path private_not_protected.pem -p 8080 -1
