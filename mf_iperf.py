#!/usr/bin/env python3
#
# Do iperf probe and send results to graphite
#
# * Run iperf3-server.sh on the remote host
# * Run this script on the local host
#
# The bitrate between the two hosts will be determined and sent to graphite
#
# Robert Hutton <rhutton@unimelb.edu.au>

import subprocess
import json
import math
import graphyte
import configparser
from subprocess import PIPE

def main():

    config = configparser.ConfigParser()
    config.read('iperf.ini')
    auth =                config.get('main', 'auth')
    username =            config.get('main', 'username')
    password =            config.get('main', 'password')
    server =              config.get('main', 'server')
    port =                config.get('main', 'port')
    rsa_public_key_path = config.get('main', 'rsa_public_key_path')

    if auth.lower() == 'true':
        command=['iperf3','-c',server,'-p',port,'-J','--rsa-public-key-path',rsa_public_key_path,'--username',username]
    else:
        command=['iperf3','-c',server,'-p',port,'-J',]

    output=subprocess.run(command,env={"IPERF3_PASSWORD": password},stdout=PIPE, stderr=PIPE)

    #print('Output:',output.stdout.decode("utf-8"))
    #print('Stderr:',output.stderr.decode("utf-8"))
    out=json.loads(output.stdout.decode("utf-8"))
    bits=math.floor(out['end']['sum_sent']['bits_per_second'])
#    print(f"""bits/sec: {bits}
#bytes/sec:{bits/8}
#kib/sec:{bits/8/1024}
#mib/sec:{bits/8/1024/1024}
#Mbits/sec:{bits/1000/1000} """)

    graphite_host =   config.get('graphite', 'graphite_host')
    graphite_prefix = config.get('graphite', 'graphite_prefix')
    graphyte.init(graphite_host, prefix=graphite_prefix)
    graphyte.send(f'iperf3.bs', bits)

if __name__ == "__main__":
    main()
